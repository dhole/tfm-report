using poly_type = nfl::poly_from_modulus<P_TYPE, N, BITS>;
using gauss = nfl::gaussian<uint8_t,
                            typename poly_type::value_type, 2>;
using fast_gauss = FastGaussianNoise<uint8_t,
                                     typename poly_type::value_type, 2>;

struct PublicKey {
    // $a$ and $b$ are stored in the NTT form for efficiency.
    poly_type a_i;
    poly_type b_i;
};

struct SecretKey {
    // $s$ is stored in the NTT form for efficiency.
    poly_type s_i;
};

struct Encryption {
    poly_type u;
    poly_type v;
};

size_t q = poly_type::get_modulus(0);
size_t q_1_2 = q/2 + 1;
size_t q_1_4 = q/4;
size_t q_3_4 = q/2 + q/4 + 1;

void
poly_from_bitvector(poly_type &p, std::vector<bool> &vec)
{
    // Since poly coefficients are 0 or 1, we can find them all at cm = 0
    for (size_t i = 0; i < p.degree; i++) {
        p(0, i) = (bool) vec[i];
    }
    return 0;
}

void
bitvector_from_poly(std::vector<bool> &vec, poly_type &p)
{
    for (size_t i = 0; i < p.degree; i++) {
        vec[i] = (bool) p(0, i);
    }
    return 0;
}

void
scale_poly_1_q2(poly_type &p)
{
    for (auto& it : p) {
        it *= q_1_2;
    }
}

void
scale_poly_q2_1(poly_type &p)
{
    for (auto& it : p) {
        if (it < q_1_4 || it > q_3_4) {
            it = 0;
        } else {
            it = 1;
        }
    }
}

void
gen_key_pair(PublicKey &pk, SecretKey &sk, gauss &gauss_s)
{
    pk.a_i = poly_type( (nfl::uniform()) );
    sk.s_i = poly_type(gauss_s);
    poly_type e(gauss_s);

    pk.a_i.ntt_pow_phi();
    sk.s_i.ntt_pow_phi();

    pk.b_i = pk.a_i * sk.s_i;
    e.ntt_pow_phi();
    pk.b_i = pk.b_i + e;
}

void
encrypt(PublicKey &pk, std::vector<bool> &msg, Encryption &enc,
        gauss &gauss_s)
{
    poly_type zq2;
    poly_from_bitvector(zq2, msg);
    scale_poly_1_q2(zq2);

    poly_type r(gauss_s);
    poly_type e1(gauss_s);
    poly_type e2(gauss_s);

    r.ntt_pow_phi();
    enc.u = pk.a_i * r;
    enc.v = pk.b_i * r;
    enc.u.invntt_pow_invphi();
    enc.v.invntt_pow_invphi();
    // No need to invntt a and b because they are local copies
    enc.u = enc.u + e1;
    enc.v = enc.v + e2 + zq2;
}

void
decrypt(SecretKey &sk, std::vector<bool> &msg, Encryption &enc)
{
    // We use a copy of u because we will be transforming (ntt) it's values
    // during the process.
    poly_type u = enc.u;
    poly_type zq2;

    u.ntt_pow_phi();
    zq2 = u * sk.s_i;
    zq2.invntt_pow_invphi();
    zq2 = enc.v - zq2;

    scale_poly_q2_1(zq2);

    bitvector_from_poly(msg, zq2);
}
