def p_error(n, q, s, sym=2):
    # sym: number of symbols
    sigma0 = s/sqrt(2*pi.n())
    sigma = sqrt(2*n*sigma0**4 + sigma0**2)
    T = RealDistribution('gaussian', sigma)
    p_err = 1 - 2*(T.cum_distribution_function(q/(sym*2)) - 0.5)
    return p_err

def bin_search(target_y, min_x, max_x, func, eps = 0.001, iter=1000000):
    eps = target_y * eps
    for i in range(iter):
        m = min_x + (max_x - min_x) / 2
        y = func(m)
        if y <= max(target_y - eps, 0):
            min_x = m
        elif y > (target_y + eps):
            max_x = m
        else:
            return m.n()
    return m.n()

def find_s(n, q, p_err, max_s):
    func = lambda s: p_error(n, q, s)
    s = bin_search(p_err, 1, max_s, func)
    return s
