from sage.modules.misc import gram_schmidt

def nearest_plane(B, w0):
    # B: lattice basis, w0: target vector
    n = B[0].degree()
    w = matrix(QQ, n)
    y = matrix(ZZ, n)
    B1, mu = gram_schmidt(list(B))
    w[n-1] = w0
    for i in range(n)[::-1]:
        li = (w[i]*B1[i]) / (B1[i]*B1[i])
        y[i] = round(li)*B[i]
        if i != 0:
            w[i-1] = w[i] - (li - round(li)) * B1[i] - round(li)*B[i]
    return sum(y)
